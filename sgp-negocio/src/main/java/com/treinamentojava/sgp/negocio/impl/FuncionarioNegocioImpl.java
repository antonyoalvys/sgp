package com.treinamentojava.sgp.negocio.impl;

import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import com.treinamentojava.sgp.entidades.Funcionario;
import com.treinamentojava.sgp.negocio.FuncionarioNegocio;
import com.treinamentojava.sgp.repositorio.FuncionarioRepositorio;

@Stateless
public class FuncionarioNegocioImpl implements FuncionarioNegocio {

	@Inject
	private FuncionarioRepositorio funcionarioRepositorio;

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public Funcionario load(Long id) {
		return funcionarioRepositorio.getById(id);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void save(Funcionario entidade) {
		funcionarioRepositorio.save(entidade);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void remove(Long id) {
		funcionarioRepositorio.delete(id);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public List<Funcionario> finByFields(Map<String, Object> fields,
			Boolean exclusive, int maxresults, String orderBy) {
		return funcionarioRepositorio.findByFields(fields, exclusive, maxresults, orderBy);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public List<Funcionario> findAll() {
		// TODO Auto-generated method stub
		return funcionarioRepositorio.findAll();
	}

}
