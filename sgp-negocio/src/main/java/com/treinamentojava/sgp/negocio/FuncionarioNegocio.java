package com.treinamentojava.sgp.negocio;

import java.util.List;
import java.util.Map;

import com.treinamentojava.sgp.entidades.Funcionario;

public interface FuncionarioNegocio {
	
	Funcionario load( Long id );
	void save( Funcionario entidade );
	void remove( Long id );
	List<Funcionario> finByFields( Map<String, Object> fields, Boolean exclusive, int maxresults, String orderBy);
	List<Funcionario> findAll();
	
}
