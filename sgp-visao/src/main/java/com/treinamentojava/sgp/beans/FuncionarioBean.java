package com.treinamentojava.sgp.beans;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import com.treinamentojava.sgp.entidades.Funcionario;
import com.treinamentojava.sgp.negocio.FuncionarioNegocio;

@Named
@ViewScoped
public class FuncionarioBean implements Serializable {

	private static final long serialVersionUID = -6390759507978686339L;

	@Inject
	private FuncionarioNegocio funcionarioNegocio;

	private Funcionario funcionario;
	private List<Funcionario> funcionarios;
	private UploadedFile file;

	@PostConstruct
	public void init() {
		this.funcionarios = funcionarioNegocio.findAll();
		clean();
	}

	public void salvar() {
		if ( file != null ) {
			funcionario.setFoto(file.getContents());
		}
		funcionarioNegocio.save(this.funcionario);
		FacesContext ctx = FacesContext.getCurrentInstance();
		clean();
		ctx.addMessage(null, new FacesMessage("Funcionário salvo com sucesso."));
	}
	
	public void consultar() {
		Map<String, Object> fields = new HashMap<String, Object>();
		if ( funcionario.getNome() != null )
			fields.put("nome", "like " + "%"+funcionario.getNome()+"%");
		if ( funcionario.getMatricula() != null )
			fields.put("matricula", funcionario.getMatricula() );
		this.funcionarios = funcionarioNegocio.finByFields(fields, true, 100, "nome");
	}
	
	public void uploadFileListener( FileUploadEvent e ) {
		if ( e.getFile() != null ) {
			this.file = e.getFile();
		}
	}
	
	public void clean() {
		this.funcionario = new Funcionario();
	}
	
	public void findAll() {
		this.funcionarios = funcionarioNegocio.findAll();
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

	public List<Funcionario> getFuncionarios() {
		return funcionarios;
	}

	public void setFuncionarios(List<Funcionario> funcionarios) {
		this.funcionarios = funcionarios;
	}

	public UploadedFile getFile() {
		return file;
	}

	public void setFile(UploadedFile file) {
		this.file = file;
	}
	
}
