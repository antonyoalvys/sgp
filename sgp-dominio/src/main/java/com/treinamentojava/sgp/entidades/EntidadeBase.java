package com.treinamentojava.sgp.entidades;

import java.io.Serializable;

public interface EntidadeBase<T> extends Serializable {

	T getId();

	void setId(T id);

}
