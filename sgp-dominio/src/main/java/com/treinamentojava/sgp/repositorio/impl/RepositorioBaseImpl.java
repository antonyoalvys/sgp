package com.treinamentojava.sgp.repositorio.impl;

import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import com.treinamentojava.sgp.entidades.EntidadeBase;
import com.treinamentojava.sgp.repositorio.RepositorioBase;

public abstract class RepositorioBaseImpl<T extends EntidadeBase<?>> implements
		RepositorioBase<T> {

	@PersistenceContext
	protected EntityManager em;
	
	@Override
	@Transactional
	public void save(T entity) {
		Object id = entity.getId();

		if (id == null)
			em.persist(entity);
		else
			em.merge(entity);

	}

	@Override
	@Transactional
	public void delete(Object id) throws EntityNotFoundException {
		em.remove(this.getById(id));
	}

	@Override
	public T getById(Object id) throws EntityNotFoundException {
		T entity = em.find(getClassT(), id);

		if (entity == null)
			throw new EntityNotFoundException();
		else
			return entity;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<T> findAll() {
		Query query = em.createQuery("from " + getClassT().getName() + " t");
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<T> findByFields(Map<String, Object> fields, Boolean exclusive,
			int maxResults, String orderBy) {

		StringBuilder strbld = new StringBuilder("from "
				+ getClassT().getName() + " t");
		String param = "";
		String connector = " where ";

		if (fields != null) {
			for (String key : fields.keySet()) {
				param = key.replace(".", "");
				if (fields.get(key) instanceof String) {
					String strfld = (String) fields.get(key);

					if (strfld.equals("is null"))
						strbld.append(connector + "t." + key + " is null");
					else if (strfld.equals("is not null"))
						strbld.append(connector + "t." + key + " is not null");
					else if (strfld.startsWith("like "))
						strbld.append(connector + "lower(t." + key
								+ ") like lower(:" + param + ")");
					else if (strfld.startsWith("!= "))
						strbld.append(connector + "t." + key + " != :" + param);
					else
						strbld.append(connector + "t." + key + "= :" + param);
				} else 
					strbld.append(connector + "t." + key + " = :" + param);
				
				if ( exclusive )
					connector = " and ";
				else 
					connector = " or ";
			}
		}
		
		if ( orderBy != null ){
			if (!"".equals(orderBy.trim())) 
				strbld.append(" order by t." + orderBy);
		}
		
		Query query = em.createQuery(strbld.toString());
		if ( maxResults > 0)
			query.setMaxResults(maxResults);
		
		param = "";
		
		for( String key : fields.keySet() ) {
			if (!fields.get(key).equals("is null") && !fields.get(key).equals("is not null")){
				param = key.replace(".", "");
				
				if (fields.get(key) instanceof String ) {
					String strfld = (String) fields.get(key);
					
					if (strfld.startsWith("like ") ) {
						strfld = strfld.replace("like ", "").replace("'", "");
						query.setParameter(param, strfld);
					} else 
						query.setParameter(param, fields.get(key));
				} else 
					query.setParameter(param, fields.get(key));
			}
		}

		return (List<T>) query.getResultList();
	}

	public abstract Class<T> getClassT();
	
}
