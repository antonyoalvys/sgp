package com.treinamentojava.sgp.repositorio;

import com.treinamentojava.sgp.entidades.Funcionario;

public interface FuncionarioRepositorio extends RepositorioBase<Funcionario> {

}
