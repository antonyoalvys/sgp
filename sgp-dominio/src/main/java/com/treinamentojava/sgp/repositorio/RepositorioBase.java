package com.treinamentojava.sgp.repositorio;

import java.util.List;
import java.util.Map;

import javax.persistence.EntityNotFoundException;

import com.treinamentojava.sgp.entidades.EntidadeBase;

public interface RepositorioBase<T extends EntidadeBase<?>> {
	
	void save( T entity );
	
	void delete( Object id ) throws EntityNotFoundException;
	
	T getById( Object id ) throws EntityNotFoundException;
	
	List<T> findAll();
	
	List<T> findByFields(Map<String, Object> fields, Boolean exclusive, int maxResults, String orderBy);
	
}
