package com.treinamentojava.sgp.repositorio.impl;

import java.io.Serializable;

import javax.enterprise.context.Dependent;
import javax.inject.Named;

import com.treinamentojava.sgp.entidades.Funcionario;
import com.treinamentojava.sgp.repositorio.FuncionarioRepositorio;

@Named
@Dependent
public class FuncionarioRepositorioImpl extends RepositorioBaseImpl<Funcionario> implements FuncionarioRepositorio, Serializable {

	private static final long serialVersionUID = 1152842030551277765L;

	@Override
	public Class<Funcionario> getClassT() {
		return Funcionario.class;
	}
	
}
